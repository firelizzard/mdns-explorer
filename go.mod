module gitlab.com/firelizzard/mdns-explorer

go 1.12

require (
	github.com/cenkalti/backoff v2.1.1+incompatible // indirect
	github.com/grandcat/zeroconf v0.0.0-20190118114326-c2d1b4121200
	github.com/miekg/dns v1.1.6 // indirect
	golang.org/x/net v0.0.0-20190320064053-1272bf9dcd53 // indirect
	golang.org/x/sync v0.0.0-20190227155943-e225da77a7e6 // indirect
)

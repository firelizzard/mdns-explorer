package main

import (
	"context"
	"flag"
	"log"
	"time"

	"github.com/grandcat/zeroconf"
)

var (
	service  = flag.String("service", "_services._dns-sd._udp", "Set the service category to look for devices.")
	domain   = flag.String("domain", "local", "Set the search domain. For local networks, default is fine.")
	waitTime = flag.Int("wait", 10, "Duration in [s] to run discovery.")
)

func main() {
	flag.Parse()

	// Discover all services on the network (e.g. _services._dns-sd._udp)
	resolver, err := zeroconf.NewResolver(nil)
	if err != nil {
		log.Fatalln("Failed to initialize resolver:", err.Error())
	}

	entries := make(chan *zeroconf.ServiceEntry)
	go func() {
		for e := range entries {
			log.Printf("instance: %s, service: %s, domain: %s, host: %s, port: %d, ipv4: %v, ipv6: %v, text: %v", e.Instance, e.Service, e.Domain, e.HostName, e.Port, e.AddrIPv4, e.AddrIPv6, e.Text)
		}
		log.Println("No more entries.")
	}()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(*waitTime))
	defer cancel()
	err = resolver.Browse(ctx, *service, *domain, entries)
	if err != nil {
		log.Fatalln("Failed to browse:", err.Error())
	}

	<-ctx.Done()
	// Wait some additional time to see debug messages on go routine shutdown.
	time.Sleep(1 * time.Second)
}
